#include <stdio.h>


int square(int x) {
    return x*x;
}


void main() {
    int v1;
    int v2;

    v1 = square(2);
    v2 = square(5);
    
    printf("V1 : %d | V2 : %d\n", v1, v2);
    
}
